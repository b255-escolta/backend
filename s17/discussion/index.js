console.log("Hello World");


	// Functions in javascript are lines/blocks of codes that tell our device or app to perform a certain taks.
	// Functions are mostly created to create complicated tasks to run several lines of code in succession


// FUnctions Declaration

	function printName (){
	console.log("My name is John");
	};

// Function Invocation

	printName();

// Function Declarations vs Expressions

	// Function declaration

	// A function can be created thru function delcaration by using the function keyword and adding a function name

	declaradFunction();

	function declaradFunction(){
	console.log("Hello World from declaradFunction()")
	};

	declaradFunction();


	
	// Function Expression
	// A function can also be stored in a variaable. This is called a function expression

	//  function expression is an anonymous function assigned to the cariable function
	// anonymous function - a function without a name

	let variableFunction = function (){
		console.log("Hello Again");
	}

	variableFunction();

	// we can also create a function expression of a name function. however to invoke the function expression, we invoke it by its variable name . not by its function aname

	let funcExpression = function funcName(){
		console.log("Hello from the other side");
	}

	funcExpression();

	// you can re assign declared functions and function epression to  a new anonymouse functions

	declaradFunction = function(){
		console.log("updated declaradFunction");
	}

	declaradFunction();

	funcExpression = function (){
		console.log("updated funcExpression");
	}

	funcExpression();

	// however, we cannot reassign a function expression inittialized with const

	const constantFunc = function (){
		console.log("Initialized with const!");
	}

	constantFunc();

	// constantFunc = function (){
	// 	console.log("cannot be reassigned!");
	// }

	// constantFunc();

	// Function Scoping
/*
	scope is the accessibility (visibility) of variables within our programs

	Javascript variables has 3 types of scopes:
	1. local/block scope
	2. global scope
	3. function scope
*/

	{
		let localVar = "Armando Perez";
	}

		let globalVar = "Mr. Worldwide";

	console.log(globalVar);

	// Functions Scope

	function showNames(){
		var functionVar = "Joe";
		const functionCost = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionCost);
		console.log(functionLet);
	}

		showNames();


	// Nested functions
		// you can create another function inside a function. this is called a nested function.

		function myNewFunction(){
			let name = "Jane";

			function nestedFunction(){
				let nestedName = "John";
				console.log(name);
			}

			// console.log(nestedName);
			nestedFunction();
		}

		myNewFunction();


	// fucntion and global scoped variables

		// global scope vairables

		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz";
			console.log(globalName);
		}

		myNewFunction2();

	// using alert()
		// allowas us to show a small window at the top of page to show info to our users. as oppoes to comsole.log()

		alert("Hello World");

		function showSampleAlert (){
			alert("Hello, User");
		}

		showSampleAlert();

		// console.log("Waiting");

		console.log("I will only log in the console when the alert is dismissed");

// Using Prompt()
		// prompt allows us to show a small window at the browser to gather user input.

		let samplePrompt = prompt ("Enter your Name: ");

		console.log("Hello, " +samplePrompt);

		let sampleNullPrompt = prompt("dont enter anything.");

		console.log("sampleNullPrompt");
		// returns an empty string when there is no input or null if the users cancels the prompt().

		function printWelcomeMessage(){
			let firstName = prompt("enter your first name: ");
			let lastName = prompt("enter your last name: ");
			console.log("Hello, " +firstName + " " + lastName + "!");
			console.log("Welcome to my page!");
		}

		printWelcomeMessage();


// function naming convention
		// function names shuld be definitive of the task it will perform. it usually contains a verb.

		function getCourses(){
			let courses = ["science 101", "math 101", "english 101"];
			console.log(courses);
		}

		getCourses();

		// avoid eneric names to avoid confusion

		function get(){
			let name = "Jaime";
			console.log(name);
		}

		get();

		// avoid pointless and inappropriate functions names 

		function foo() {
			console.log(25%5);
		}

		foo();

		// name your function in small caps follow camelcase when naming variable and functions

		function displayCarInfo (){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,00");

		}

		displayCarInfo();




