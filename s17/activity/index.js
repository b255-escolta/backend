/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:


function printUserInfo(){


			let firstName = prompt("Enter your First Name: ");
			let lastName = prompt("Enter your Last Name: ");
			const age = prompt("Enter your Age: ");
			let street = prompt("Enter your Street: ");
			let city = prompt("Enter your City: ");
			let cat = prompt("Enter your Cat's Name: ");
			let dog = prompt("Enter your Dog's Name: ");
			

			console.log("Hello, I'm " +firstName + " " + lastName + ".");
			console.log("I am " +age +" years old.");
			console.log("I live in " +street + " " + city + ".");
			console.log("I have a cat named " +cat +".");
			console.log("I have a dog named " +dog +".");

}

printUserInfo();







/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:

function printFiveBands(){


			let bandOne = "The Beatles";
			let bandTwo = "Taylor Swift";
			let bandThree = "The Eagles";
			let bandFour = "The Rivermaya";
			let bandFive = "Eraserheads";
			

			console.log(bandOne);
			console.log(bandTwo);
			console.log(bandThree);
			console.log(bandFour);
			console.log(bandFive);
			

}

printFiveBands();




/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:

function printFiveMovies(){


			let movieOne = "Lion King";
			let movieTwo = "Howl's Moving Castle";
			let movieThree = "Meet the Robinson's";
			let movieFour = "School of Rock";
			let movieFive = "Spirited Away";
			

			console.log(movieOne);
			console.log(movieTwo);
			console.log(movieThree);
			console.log(movieFour);
			console.log(movieFive);
			

}

printFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}