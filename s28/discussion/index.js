// CRUD Operations
// Insert Document (CREATE)

/*
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"

			})

		Insert Many Documents
		db.collectionName.insertMany([
		{
				"fieldA": "valueA",
				"fieldB": "valueB"
		},
		{
				"fieldA": "valueA",
				"fieldB": "valueB"
		}
	]);
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"company": "none"
});

db.users.insertMany([
	{
	"firstName": "Stephen",
	"lastName": "Hawking",
	"age": 76,
	"email": "stephenhawking@mail.com",
	"company": "none"
	},

	{
	"firstName": "Neil",
	"lastName": "Armstrong",
	"age": 82,
	"email": "neilarmstrong@mail.com",
	"company": "none"
	}
])

// FIND Documents (Read/Retrieve)

db.users.find();

db.users.find({
	"firstName": "Jane"
});

db.users.findOne({}); //returns the first document in our collection

db.users.findOne({
	"firstName": "Stephen"
});

// Update Documents

db.users.insertOne({
	
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"company": "none"
	
});


// updating one document

db.users.updateOne(
{
	"firstName": "Test"
},
{
	$set:{
		
	"firstName": "Bill",
	"lastName": "Gates",
	"age": 65,
	"email": "billgates@mail.com",
	"department": "operations"
	"status": "active"
	
	}
}
);

// Removing a field
db.users.updateOne(
	{
		"firstName": "Bill"
	},

	{
		$unset:{
			"status": "active"
		}
	}
);

// Updating multiple documents
db.users.updateMany(
{
	"company": "none"
},
{
	$set:{
		"company": "HR"
	}
}
);

db.users.updateOne(
	{},
	{
		$set:{
			"company": "operations"
		}
	}

	);

// Deleting Documents (Delete)
db.users.insertOne({
	"firstName": "Test"
});

db.users.deleteOne({
	"firstName": "Test"
});

db.users.deleteMany({
	"company": "comp"
});

db.courses.deleteMany({});






db.courses.insertMany([
		{
			"name": "Javascript101",
			"price": 5000,
			"description":"Introduction to Javascript",
			"isActive": true,
		},
		{
			"name": "HTML 101",
			"price": 2000,
			"description":"Introduction to HTML",
			"isActive": true,
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"description":"Introduction to CSS",
			"isActive": true,
		}
	]);