db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
});

db.users.insertMany([
	{
	"firstName": "Stephen",
	"lastName": "Hawking",
	"age": 76,
	"email": "stephenhawking@mail.com",
	"department": "none"
	},

	{
	"firstName": "Neil",
	"lastName": "Armstrong",
	"age": 82,
	"email": "neilarmstrong@mail.com",
	"department": "none"
	}
]);

db.users.find(
	{ 
		$or: [{firstName: {$regex: "S"}}, {lastName: {$regex: "D"}}]
	},
	{
		_id: 0,
		firstName: 1,
		lastName: 1
	}

	);


db.users.find({ $and: [{department: "none"}, {age: {$gte: 70}}]})

db.users.find({ $and: [{firstName: {$regex: "e"}}, {age: {$lte: 30}}]})


