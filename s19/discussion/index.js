console.log("Hello World");

// Conditional Statements allow us to control the flow of our program
// It allows us to run a statement/instruction if a condition if met or run another separate instruction if otherwise

// [SECTION] if, else if, else statement
let numA = -1;

if(numA < 0){
	console.log('Hello');
}

// The result of the expresion added in the if's condition must result to true, else, the statement inside if() will not run
// You can also check the condition. The expression results to a boolean true because of the use of the less than operator

console.log(numA<0); //results to true and som the if statement was  run

numA = 0;

if (numA < 0){
	console.log("Hello again if numA is 0!")
}

// it will not run because the expression now results to false
console.log (numA < 0 );

let city = "New York";

if (city === "New York"){
	console.log("Welcome to New York City!");
}

// Else if clause

/*
	-Executes a statement if previous conditions are flase and if the specified conditions is true
	-The "else if" clause is optional and can be added to capture additional condifitons to change the flow of a program

*/

let numH = 1;

if (numA < 0){
	console.log('Hello');
} else if (numH > 0){
	console.log("World");
}

// we were able to run the else if() statement after we evaluated that the if condition was failed
// If the if() condition was passed and run, we will no longer evaluate the else if() and end the process there.

numA = 1;

if (numA > 0 ){
	console.log('Hello');
} else if (numH > 0 ){
	console.log('World');
}

// else if() statement no longer ran because the if statement

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City");
} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}

// Since we failed the condition for the first if(), we went to the else if () and checked and passed that condition

// Else statement
/*
	-Executes a statement if all other conditions are false
	-The else statement is optional and can be added to capture any other result to change the flow of the program
*/

if(numA < 0){
	console.log('Hello');
} else if (numH === 0 ){
	console.log('World');
} else {
	console.log('Error! numA is not less than 0');
}

// else statements should only be added if there is a preceeding if condition. else statements by itself will not work, however, if statement will work even if there is no else statement

// if, else if and else statements with functions
/*
	-most of the time we would like to use if, else if and else statements with functions to control the flow of our application
	-by including them inside functions, we can decide when certain conditions will be checked isntead of executing statements when the JavaScript loads
	-The return statement can be utilized if conditional statements in combination with functions to change values to be used for other features
*/

let message = 'No message';
console.log(message);

function determineTyphoonIntensity(windspeed){
	if (windspeed < 30){
		return 'Not a typhoon yet';
	} 

	else if (windspeed <= 61){
		return 'Tropical depression detected';
	}

	else if (windspeed >= 62 && windspeed <=88){
		return 'Tropical storm detected';
	}

	else if (windspeed >= 89 || windspeed <= 117 ){
		return 'Severe tropical storm detected';
	}

	else{
		return 'Typhoon detected';
	}
}	

message = determineTyphoonIntensity(25);
console.log(message);

// mini activity


function determineNum(checkNum){
	if ((checkNum % 2) === 0){
		return 'Even';
	} 

	else{
		return 'Odd';
	}
}	

message = determineNum(3);
console.log(message);

// truthy examples

/*
	-If the result of an expression in a condition results to a truthy value, the conditions returns true and the corresponding statements are executed
	-Expressions are any unit of code that can be evaluated to a value
*/

if (true){
	console.log('Truthy');
}

if (1){
	console.log('Truthy');
}

if ([]){
	console.log('Truthy');
}

// falsy examples

if (false){
	console.log('Falsy');
}

if (0){
	console.log('Falsy');
}

if (undefined){
	console.log('Falsy');
}

// [SECTION] Conditional (ternary) Operator

	/*
	-the conditional (ternary) operator takes in three operands
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy

	-can be used as an alternative to an "if else" statement
	-commonly used for single statement execution where the result consists of only one line of code
	
	Syntax:
	(expression) ? ifTrue : ifFalse;

	*/

	// single statement execution

	let ternaryResult = (1<18) ? true : false;
	console.log("result of ternary operator: " +ternaryResult);

	// multi statement execution

	let name;

	function isOfLegalAge(){
		name ='John'
		return 'You are of the legal age limit'
	}

	
	function isUnderAge(){
		name ='Jane'
		return 'You are under the age limit'
	}

	// input received from the prompt function is returned as a string data type.
	// the "parseInt" function converts the input received into a number data type
	// NaN (not a number)

	let age = parseInt(prompt ("What is your age?"));
	console.log(age);
	let legalAge = (age > 18) ? isOfLegalAge(): isUnderAge();
	console.log("result of ternary operator in functions: " +legalAge + ', ' +name);

// [SECTION] Switch Statements
/*
	-The switch statement evaluates an expression and matches the expressions value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the match.

	-switch cases are considered are "loops" meaning it will compare the expression with each of the case values until a match is found

	-the "break" statement is used to terminate the current loop once a match has been found

*/

let day = prompt("What day of the week is it today?") .toLowerCase();
console.log(day);

switch(day){
	case 'monday':
		console.log("The color of the day is red");
		break;

	case 'tuesday':
		console.log("The color of the day is orange");
		break;

	case 'wednesday':
		console.log("The color of the day is yellow");
		break;

	case 'thursday':
		console.log("The color of the day is green");
		break;

	case 'friday':
		console.log("The color of the day is blue");
		break;

	case 'saturday':
		console.log("The color of the day is indigo");
		break;

	case 'sunday':
		console.log("The color of the day is violet");
		break;

	default:
		console.log("Please input valid day");
		break;
}

// [SECTION] Try-catch-finally statement

/*
	-"try catch" statements are commonly used for error handling
	-there are instances when the application returns an error/warning that is not necesasrily an error in the context of our code
	-These errors are a result of an attempt of the programming language to help developers in creating efficient code
	-they are used to specify a response whenever an exception/error is received
*/

function showIntensityAlert(windspeed){
	try{
				alerat(determineTyphoonIntensity(windspeed));
			} catch (error){
				console.log(typeof error);
				console.warn(error.message);
			}	finally{
				alert('Intensity updates will show new alert');
			}

		}	

showIntensityAlert(56);

// error/err are commonly used variable names used by developers for storing errrors
// the typeof operator is used to check the data type of a value/expression abd returns a tring value of what the data type is
// catch error within the try statement
// in this case the error is an unknown function 'alerat' which does not exist in javascript
// "error.message" is used to access the information relating to an error object
// continie execution of code regardless of success or failure of code execution in the 'try' block to handle/resolve errors