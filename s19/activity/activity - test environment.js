console.log("Hello World");

/*
    
    1. Create a function called login which is able to receive 3 parameters called username,password and role.
        -add an if statement to check if the the username is an empty string or undefined or if the password is an empty string or undefined or if the role is an empty string or undefined.
            -if it is, return a message in console to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, return the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher,return the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie,return the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, return a message:
                    "Role out of range."
*/

let username = prompt("Input your username: ").toLowerCase();
let password = prompt("Input your password: ").toLowerCase();
let role = prompt("Input your role: ").toLowerCase();

function login(){

	if(username==='' || password==='' || role===''){
		alert("Inputs must not be empty!");
	}

	else {
		switch(username,password,role){
		case 'adminuser' , 'password' , 'admin':
			console.log("Welcome back to the class portal, admin!");
			break;

		
		case 'teacheruser' , 'password' , 'teacher':
			console.log("Thank you for logging in, teacher!");
			break;

		
		case 'studentuser' , 'password' , 'student':
			console.log("Welcome to the class portal, student!");
			break
		
		default:
			console.log("Role out of range");
			break

	}

}
}
message = login();

// let username;
// let password;
// let role;

// function login(username, password, role){
// 	username = prompt("Input your username: ").toLowerCase();
// 	password = prompt("Input your password: ").toLowerCase();
// 	role = prompt("Input your role: ").toLowerCase();

// 	console.log(`login:(${username}, ${password}, ${role})`);
// 	if (username === '' || password === '' || role === ''){
// 		console.log("Inputs must not be EMPTY!");
// 	} else {
// 		switch(role){
// 		case 'admin':
// 			console.log(`Welcome back to the class portal, ${role}!`);
// 			break

// 		case 'teacher':
// 			console.log(`Thank you for logging in, ${role}!`);
// 			break

// 		case 'student':
// 			console.log(`Welcome to  the class portal, ${role}!`);
// 			break

// 		default:
// 			console.log('Role out of range!');

// 		}

// 	}
// }
// login(username, password, role);


/*
    2. Create a function called checkAverage able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

    Note: strictly follow the instructed function names.
*/


let num1 = parseInt(prompt("What is your first grade?"));
let num2 = parseInt(prompt("What is your second grade?"));
let num3 = parseInt(prompt("What is your third grade?"));
let num4 = parseInt(prompt("What is your fourth grade?"));

let checkAverage = Math.round((num1 + num2 + num3 + num4)/4);

if (checkAverage <= 74){
console.log("Hello Student, your average is: " + checkAverage + " The letter equivalent is F")
}

else if (checkAverage >= 74 && checkAverage<=79){
console.log("Hello Student, your average is: " + checkAverage + " The letter equivalent is D")
}

else if (checkAverage >= 80 && checkAverage<=84){
console.log("Hello Student, your average is: " + checkAverage + " The letter equivalent is C")
}

else if (checkAverage >= 85 && checkAverage<=89){
console.log("Hello Student, your average is: " + checkAverage + " The letter equivalent is B")
}

else if (checkAverage >= 90 && checkAverage<=95){
console.log("Hello Student, your average is: " + checkAverage + " The letter equivalent is A")
}

else if (checkAverage >= 96 && checkAverage<=100){
console.log("Hello Student, your average is: " + checkAverage + " The letter equivalent is A+")
}

else {
	console.log("Hello Student, your average is: Invalid")
}


//Do not modify
//For exporting to test.js
try {
    module.exports = {
       login, checkAverage
    }
} catch(err) {

}