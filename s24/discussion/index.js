console.log("Hello World");

// [SECTION] Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(firstNum);

// [SECTION] Template Literals
/*
	-Allows to write strings without using the concatenation operator
	-greatly helps with code readability
*/

let name = "John";

// Pre template literal string

let message = 'Hello ' + name +'! Welcome to programming';
console.log("Message without template literals:" +message);

// template literals

message = `Hello ${name}! Welcome to programming`;
console.log(`Message with template literals: ${message}`);

// Multi line using template literals

const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8**2 with the solution of ${firstNum}.
`
console.log(anotherMessage);

/*
	-template literals allows us to write strings with embedded javascript expressions
	-expressions are any valid unit of code that resolves to a value
	- "${}" are used to include javascript expressions in strings using template literals

*/

const interestRate = .1;
const principal = 1000;

console.log(`The interest rate on your savings account is: ${principal * interestRate}`);

// [SECTION] Array destructuring
/*
	-allows to unpack elements in arrays into disctinct variables
	-allows us to name array elements with variables instead of using index numbers
	-helps code readability
*/

const fullName = ["Juan", "Dela", "Cruz"];
// pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]} It's nice to meet you!`);

// array destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName} It's nice to meet you!`);

// [SECTION] Object destructuring
/*
	-allows to unpack properties of objects into distinct variables
	-shortens the syntax for accessing properties from objects
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

// pre object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`);

// object destructuring
const {givenName,maidenName,familyName} = person;
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`);

function getFullName ({givenName,maidenName,lastName}){console.log(`${givenName} ${maidenName} ${familyName}`)}
getFullName(person);

// [SECTION] Arrow Functions
/*
	-compact alternatives suntax to traditional funcstions 
	-useful for code snippers
*/

const hello = () => {
	console.log("Hello World");
}

// arrow functions
function functionName (parameterA, parameterB, parameterC){
	console.log();
}

let variableName = (parameterA, parameterB, parameterC) =>{
	console.log();
}

const printFullName = (firstName, middleInitial, lastName) =>{
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName ("John","D.","Smith");

const students = ["John", "Jane", "Judy"];
// pre-arrow function
students.forEach(function(students){
	console.log(`${students} is a student`)
})

// arrow function
students.forEach((students) => {
	console.log(`${students} is a student`)
})

// [SECTION] Implicit return
/*
	-can omit the return statement
*/

const add = (x,y) => x+y;
let total = add(1,2);
console.log(total);

// [SECTION] Default function argument value
/*
	-provides a default argument
*/

const greet = (name = 'User') => {
	return `Good Morning, ${ name}!`
}
console.log(greet())
console.log(greet('John'))

// [SECTION] Class based object blueprints
/*
	-allows creation/instantation of objects using classes as blueprints
*/

// Creating a class

/*
	-the constructor is a special method of a class for creating/initializing an object for that class
	-"this" keyword refers to the properties of an object created/initializr fron the class
	-by using "this" keyword and accesing the objects properties,this allow us to reassign its values
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// initializing an object
/*
	- the "new operator creates a new object wth the given argument as the value of its properties"
*/

const myCar = new Car();
console.log(myCar);

// values of properties may be assigned after creation of an object

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = "2021";
console.log(myCar);

// creating a new object from the car class with nitiaized values

const myNewCar = new Car("Toyota","Vios","2021");
console.log(myNewCar);