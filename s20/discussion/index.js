console.log("Hello World");

// [SECTION] While Loop
/*
	-A while loop takes in an expression/condition
	-expressions are any unit of code that can be evaluated to a valu
	-if the condition evaluates to true, the statements inside the code block will be executed
	-a statement is a command the programmer gives to the computer
	-a loop will itirate a certain number of times until an expresion/condition is met
*/

let count = 5;

while (count !==0){
	console.log("While: " +count);
	count--;
}

// [SECTION] Do while loops
/*
	-a do while loops work a lot like the while loops. But unlike the while loops, do while loops guarantee that the code will be executed at least once
*/

// let number = Number(prompt("Give me a number: "))

// do {
// 	console.log("Do while: " +number)

// 	number +=1;
// }

// while (number <10);

// [SECTION] For loops

/*
	- a for loop is more flexible than while and do-while loops.

	1. the initialization value that will track the progression of the loop
	2. the expression / condition that will be evaluated which will determine whether the loop will run one more time
	3. the final expression indicates how to advance the loop
*/

// for (let count = 0; count <=20; count++){
// 	console.log(count);
// }


// characters in strings may be counted using the .length property. binibilang characters
let myString = "alex";
console.log(myString.length)             

// accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for (let x=0; x<myString.length; x++){
	console.log(myString[x]);
}

// Mini activity
// write for loop that will print numbers from 1-10. must be inside a function



// function countAll(){

// 	for (let count = 1; count <=10; count++){
//  	console.log(count);
//  		}

// }

// countAll();

console.log("START OF VOWEL LOOP");

let myName = "ALeX";

for (let i = 0; i<myName.length; i++){
	// console.log(myName[i].toLowerCase())

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 

		)

		{
			console.log(3);
		} else {
			console.log(myName[i]);
		}
}

// [SECTION] Continue and Break Statements

/*
	-the continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements
	-the break statement is used to terminate the current loop once a match has been found
*/

for(let count=0; count<=20; count++){
	if (count % 2 === 0){
		continue;
	}

	console.log("Continue and break: " +count);

	if (count > 10){
		break;
	}
}

let name = "alexandro";

for (let i=0; i<name.length; i++){
	console.log(name[i])

	if(name[i].toLowerCase() === 'a'){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] == "d"){
		break;
	}
}