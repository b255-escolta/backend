// console.log("Hello World");

//Objective 1
// Create a function called printNumbers that will loop over a number provided as an argument.
	//In the function, add a console log to display the number provided.
	//In the function, create a loop that will use the number provided by the user and count down to 0
		//In the loop, create an if-else statement:

			// If the counter number value provided is less than or equal to 50, terminate the loop and exactly show the following message in the console:
				//"The current value is at " + count + ". Terminating the loop."

			// If the counter number value is divisible by 10, skip printing the number and show the following message in the console:
				//"The number is divisible by 10. Skipping the number."

			// If the counter number  value is divisible by 5, print/console log the counter number.

let getNum = prompt("Enter a number: ");
console.log("The number you provided is " +getNum)

function printNumbers(i){

for(let i=getNum; i>=0; i--){

	if ((i % 10) === 0){
		console.log("The number is Divisible by 10. Skipping the number.");
	}

	if (((i % 5) === 0) && i % 10 !==0){
		console.log(+i);

	}

	if (i <= 50){
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}

}
}

printNumbers();






//Objective 2

let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

	for(let i = 0; i < string.length; i++ ){
		if(string[i] === 'a' || string[i] === 'e' || string[i] === 'i' || string[i] === 'o' || string[i] === 'u'){
			continue;
		}
		filteredString += string[i];
	}
	console.log(filteredString);






//Do not modify
//For exporting to test.js
try {
    module.exports = {
       printNumbers, filteredString
    }
} catch(err) {

}