/*
1.What directive is used by Node.js in loading the modules it needs?
Answer: require directive

2. What Node.js module contains a method for server creation?
Answer: http.createServer()

3. What is the method of the http object responsible for creating a server using Node.js?
Answer: http.createServer() method

4. What method of the response object allows us to set status codes and content types?
Answer: res.writeHead

5. Where will console.log() output its contents when run in Node.js?
Answer: res.end()

6. What property of the request object contains the address' endpoint?
Answer: port

*/



const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {
	if (request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the login page.')
	} else if (request.url == '/'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('This is the homepage')
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Im sorry the page you are looking for cannot be found')
	}

});

server.listen(port);

console.log(`Server now accesible at localhost:${port}`);