const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
// Ivokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// S38 ACTIVITY
// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	// Uses the "decode" method defined in the "auth.js" file to retrieve user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});

router.post("/enroll", auth.verify, (req, res) => {

let data = {
	// User ID will be retrieved from the request header
	userId : auth.decode(req.headers.authorization).id,
	// p ID will be retrieved from the request body
	productId : req.body.productId
}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

});



// Allows us to export the "router" object that will be acessed in our "index.js" file
module.exports = router;
