const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
			
			email : {
				type : String,
				required : [true, "Email is required"]
			},
			password : {
				type : String,
				required : [true, "Password is required"]
			},
			isAdmin : {
				type : Boolean,
				default : false
			},
			
			orderedProduct : [{

					products :{
						productId : {
							type : String,
							required : [true, "productId is required"]
							},

						productName : {
							type : String,
							default : ""
							},

						quantity : {
							type : Number,
							default : [true, "quantity is required"]
							}
					},

					totalAmount : {
						type : Number,
						default : 0
						},

					purchasedOn : {
						type : Date,
						default : new Date
						}
			}]
		})

		module.exports = mongoose.model("User", userSchema);
