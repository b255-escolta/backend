const Product = require("../models/Product");

// Create a new p
/*
	Steps:
	1. Create a new p object using the mongoose model and the information from the request body and the id from the header
	2. Save the new p to the database
*/

// S39 activity
module.exports.addProduct = (reqBody) => {
	// Create a variable "p" and instantiates a new "p" object using the mongoose model
	let newProduct = new Product({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	// Saves the created object to our database
	return newProduct.save().then((product, error) => {
		// p creation failed
		if(error) {
			return false;

		// p creation successful
		} else {
			return true
		}
	})
}

// Retrieve all p
/*
	Steps:
	1. Retrieve all the p from the database
*/
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// Retrieve all ACTIVE p
/*
	Steps:
	1. Retrieve all the p from the database with the property of "isActive" to true
*/
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrieving a specific p
/*
	Steps:
	1. Retrieve the p that matches the p ID provided from the URL
*/
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
}

// Update a p
/*
	Steps:
	1. Create a variable "p" which will contain the information retrieved from the reqeust body
	2. Find and udpate the p using the p ID retrieved from the params property and the variable "p" containing the information from the request body
*/

// Information to update a p will be coming from both the URL parameters and the request body
module.exports.updateProduct = (reqParams, reqBody) => {

	// Specify the fiels/properties of the document to be updated
	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) =>{
		// p is not updated
		if(error){
			return false;
		// p updated successfully
		}else{
			return true;
		}
	})
}



// Archive a p
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the p "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active p are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		// p not archived
		if (error) {

			return false;

		// p archived successfully
		} else {

			return true;

		}

	});
};
