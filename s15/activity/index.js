// console.log("Hello World");

/*
	1. Create the following variables to store to the following user details:

	Variable Name - Value Data Type:
	
	firstName - String
    lastName - String
    age - Number
    hobbies - Array
    workAddress - Object



	The hobbies array should contain at least 3 hobbies as Strings.
	
	The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note: strictly follow the variable names.
*/

	//Add your variables and console log for objective 1 here:

let firstName = "John";
let lastName = "Smith";
let age = 40;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];

let workAddress = {
	houseNumber: 32,
	street: 'Washington',
	city: 'Lincoln',
	state: 'Nebraska'
}


let showFirst = 'First Name: '+firstName;
console.log(showFirst);

let showLast = 'Last Name: '+lastName;
console.log(showLast);

let showAge = 'Age: '+age;
console.log(showAge);

let hobby = 'Hobbies:';
console.log(hobby);

console.log(hobbies);

let work = 'Work Address:';
console.log(work);

console.log(workAddress);





/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = +age;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are:")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	let lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);



	//Do not modify
	//For exporting to test.js
	try{
		module.exports = {
			firstName, lastName, age, hobbies, workAddress,
			fullName, currentAge, friends, profile, fullName2, lastLocation
		}
	} catch(err){
	}
