const express = require("express");
const mongoose = require("mongoose");
// creates an app variable that stores the result of the "express" function that initialize our express application

const cors = require("cors");
// allows access to routes defined within our application
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");




const app = express();

// connect to our mongodb database
mongoose.connect("mongodb+srv://gerry-255:admin123@zuitt-bootcamp.oeebhge.mongodb.net/?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));


app.use(express.json());
app.use(express.urlencoded({extended:true}));


// defined the /users string to be included for all user routes defined in the user route file
app.use("/users", userRoutes);
// defines the "/courses" string to be included for all routes defined in the "course"
app.use("/courses", courseRoutes);



if(require.main === module){

	// will used the define port number for the application whenever an environment variable is available or will use port 4000 if none is defined
	// this syntax will allow flexibility when using the app locally or as a hosted application

	app.listen(process.env.PORT || 4000, () => {
		console.log(`API is now online on port ${process.env.PORT || 4000}`)
	});
}

module.exports = app;