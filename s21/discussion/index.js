console.log("Hello World");

// an array in programming is simply a list in data.

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// [SECTION] Arrays

/*
	-Arrays are used to store multiple related variable in a single variable
	-they are declared using square brackets[] also known as "array literals"
	-commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
*/

// common examples for arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

// possible use of array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// alternative way to write arrays
let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
	];
// console.log(myTasks);


// creating an array with values from variables
let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];
console.log(myTasks);
console.log(cities);

// [SECTION] length property
// the length property allows us to get and set the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// length property can also be used with strings. some array methods and properties can also be used with strings.

let fullName = 'Jaime Noble';
console.log(fullName.length);

// length property can also set the total number of items in an array, we can actually delete the last item in the array

myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

// to delete a specific item in an array we can employ array methods whoch will be shown in the next session

// another example of using decrement
cities.length--;
console.log(cities);

fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

// if you can shorten the array by setting the length property, you can also lengthen it by adding a number into the length property

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

// [SECTION] reading from arrays
/*
	-accessing array element is one of the more common tasks that we do with an array. this can be done thru the use of array indexes
	-each element in an array is associated with its own index/number
	-in javascript, the firt element is associated with number 0 and increasing number by 1 for every proceeding element
	-the reason an array starts with 0 is due to how the language is designed
	-array indexes actually refer to an address or location in the devices memory and how the information is stored
	-example array location in memory
		Array[0] = 0X7ffe947bad0
		Array[0] = 0X7ffe947bad4
		Array[0] = 0X7ffe947bad8
*/

console.log(grades[0]);
console.log(computerBrands[0]);

// accessing an array element that does not exist will return "undefined"
console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

// we can save/store array items in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// you can also reassign array values using the items indices
console.log('Array before reassignment');
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log('Array after reassignment');
console.log(lakersLegends);

// accessing the last element of an array
// since the first element of an array starts with 0, subracting 1 to the length of an array will offset the value by one allowing us to get the last index

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);

// you can also add it directly
console.log(bullsLegends[bullsLegends.length-1]);

// adding items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

// you can also add items at the end of the array
newArr[newArr.length-1] = "Aerith Gainsborough";
newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

// Looping over an array
// you can use for loop to itirate over all items in an array
// set the counter as the index and set a condition that as long as the current indexed iterated is less than the length of the array

for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40];
for (let index = 0; index < numArr.length; index++){
	if (numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}

	else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// [SECTION] Multidimensional Array
/*
	-multidimensional arrays are useful for storing complex data structures
	-a practical application of this is to help visualize/create real world objects
	-though useful in a number of cases, creating complex array structures is not always recommended

*/

let chessBoard = [
		['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
	]

console.log(chessBoard);

// accessing elements of a multidimensional array
console.log(chessBoard[1][4]);
console.log("Pawn moves to :" +chessBoard[1][5]);