console.log("Hello World");

// Functions
	// Parameters & Arguments
	// FUnctions in Javascript are line/blocks of codes that tell our device/application to perform certain task when they are called.
	// Functions are mostly created to created complciated tasks to run several lines of codes in succession.
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function.

	function printInput(){
		let nickname = prompt ("Enter your nickname: ");
		console.log ("Hi, " +nickname);
	}

	// printInput();

	// For other cases, functions can also process data directly passed into it instead of relying on global variables and prompt()

	function printName(name){
		console.log("My name is " +name);
	}
	printName("Juana");
	printName("Juan");
	printName(2);

	// You can directly passed data into the function. The function can then call/use taht data which is reffered as "name"

	// "name" is a parameter
	// a parameter acts as a named variable/container that exist only inside of a function
	// it is used to store information that is provided to a function when it is called/invoke


	// Variables can also be passed as an argument
	let sampleVariable = "Yui";
	printName(sampleVariable);

	// function arguments cannot be used by a function if there are no parameters provided wihtin the function

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " +num + "divided by 8 is: " +remainder);
		let isDivisibleBy8 = remainder ===0;
		console.log("Is " +num +" divisible by 8?");
		console.log(isDivisibleBy8);

	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);

	// Functions as Arguments
	// Function parameters can also accept other functions as arguments
	// Some complex functions use other functions as arguments to perform more complicated results

	function argumentFunction (){
		console.log("This function was passed as an argument before the message was printed");
	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	// Adding & removing parentheses "()" impacts the output of Javascipt heavily
	// When a function is used with parentheses "()", it denotes invoking/calling a function
	// A function used without a parentheses is normally associated with using the function as an argument to another function

	invokeFunction(argumentFunction);

	// finding information about a function in the console.log()
	console.log(argumentFunction);

	// Using multiple parameters
	// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeding order.

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' +middleName + ' ' +lastName)
	}

	createFullName('Juan', 'Dela', 'Cruz');

	// Juan will be stored in parameter firstName
	// Dela will be stored in parameter middleName
	// Cruz will be stored in parameter lasttName

	// In Javascript, providing more/less arguments than the expected arguments will not return an error
	// Providing less arguments than the expected parameters will automatically assign an undefined value to the paramter

	createFullName('Juan', 'Dela');
	createFullName('Juan', 'Dela', 'Cruz', 'Hello');

	// Using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	// Parameter names are just names to refer to the argument. Even if we change the name of the parameters, thearguments will be receivedin the same order it was passed.

	// The return statement
	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

	function returnFullName(firstName, middleName, lastName){
		return firstName + ' ' +middleName + ' ' +lastName
		console.log("This message will not be printed");
	}

	// In our example, the "returnFullName" function was invokde/called in the same line as declaring a variable.
	// Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable

	let completeName = returnFullName("Jeffrey", "Amazon", "Bezos");
	console.log(completeName);

	// This way, a function is able to return a value we can further use/manipulate in our program instead of only printing/displaying it in the console.

	// mini-activity
	// create a function that will add to numbers together
	// you must use parameters and argument

	function createAddNumbers(num1, num2){
		return num1 + num2;
	}

	let sum = createAddNumbers(5,10);
	console.log(sum)

	// Notice that the console.log() after the return is no longer printed in the console. That is because dieally any line/block of code that comes after the return statement is ignored because it ends the functione execution.

	// In this example, console.log will print the returned value of the returnFullName() function

	console.log(returnFullName(firstName, middleName, lastName));

	// You can also create a variable inside the function to contain the result and return that variable instead.
	function returnAddress(city, country){
		let fullAddress = city + " , " +country; 
		return fullAddress;
	}

	let myAddress = returnAddress("Cebu City", "Cebu");
	console.log(myAddress);

	// On the other hand, when a functiononly has console.log() to display its results it will return undefined instead.

	function printPlayerInfo(username, level, job){
		console.log("Username: " +username);
		console.log("Level: " +level);
		console.log("Job: " +job);

	}

	let user1 = printPlayerInfo("Knight_White", 95, "Paladin");
	console.log(user1);


	// returns undefined because printPlayerInfo returns nothing. it only console.logs the details
	// you cannot save any value from printPlayerInfo because it does not returns anything