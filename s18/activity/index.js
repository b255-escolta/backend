console.log("Hello World");


function addNum(num1, num2){
		console.log(num1+num2)
	}
	console.log("Displayed sum of 5 and 15");
	addNum(5,15)

function subNum(num3, num4){
		console.log(num3-num4)
	}
	console.log("Displayed difference of 20 and 5");
	subNum(20,5)

function multiplyNum(num5, num6){
	return num5 * num6;
}
	let multiply = multiplyNum(50,10);
	console.log("The product of 50 & 10");
	console.log(multiply)

function divideNum(num7, num8){
	return (num7 / num8);
}
	let divide = divideNum(50,10);
	console.log("The quotient of 50 & 10");
	console.log(divide)

function getCircleArea(num9){
	return (3.1416 * num9**2);

}
	let rad = getCircleArea(15);
	console.log("The result of getting the area of a circle with 15 radius: ");
	console.log(rad)


function getAverage(num10, num11, num12, num13){
	return ((num10 + num11 + num12 + num13)/4);
}
	let avg = getAverage(20,40,60,80);
	console.log("The average of 20, 40, 60 and 80: ");
	console.log(avg)




function checkIfPassed(num14){
	let isPassed = (num14 / 50)*100;
	console.log ("Is " +num14 +" / 50 a passing score?");
	let isPassingScore = isPassed > 75;
	console.log(isPassingScore);
}

	checkIfPassed(38);
	





/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}