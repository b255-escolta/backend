console.log("Hello World");

// Array Methods
// Javascript has built in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator methods
/*
	-are functions that mutate or change an array afgter they are created
	-these methods manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
// push() - adds an element in the end of an array and returns the arrays length

console.log("Current array: ");
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log("Mutated array from push method");
console.log(fruits);

// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log("Mutated array from push method");
console.log(fruits);

// pop()
// removes the last element and returns the removed element

let removeFruit = fruits.pop();
console.log(removeFruit);
console.log("Mutated array from pop method");
console.log(fruits);

// pra hnd na magshow ung tinangal
fruits.pop();
console.log(fruits);

// unshift()
// add an element at the beginning of an array 

fruits.unshift('Lime', 'Banana', 'Orange');
console.log(fruits);

// shift()
// removes an elemet at the beginning of an array and returns the removed element

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

// shortcut
// fruits.shift();
// console.log(fruits);

// splice();
// simultaneously removes elements from a specified index number and adds elements

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log("Mutated array from splice method");
console.log(fruits);

// sort();
// rearranges the array elements in alphanumeric order
fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

/*
sort method is used for more complicated sorting functions
*/

// reverse()
/*
reverses the order of array elements
*/

fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);



// Non mutator methods
/*
are functions that do not modify or change an array after theyre created. these methods do not manipulate original array performing various tasks such as returnnig element from an array and combining array
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf()
/*
	returns the index number of the first matching element found in an array.

	if no match is found, result will be -1

	the search process will be done from first to last element
*/

let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' +firstIndex);

let invalidCountry = countries.indexOf('BR')
console.log('Result of indexOf method: ' +invalidCountry);

// lastIndexOf();
// returns the index number of the last matching element found in an array

let lastIndex= countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' +lastIndex);

let lastIndexStart = countries.lastIndexOf('PH', 3);
console.log('Result of lastIndexOf method: ' +lastIndexStart);



// slice()
// portions/slice elements from an array and returns a new array

let slicedArrayA = countries.slice(2);
console.log('Result of slice method: ');
console.log(slicedArrayA);

console.log(countries);

let slicedArrayB = countries.slice(2, 4);
console.log('Result of slice method: ');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result of slice method: ');
console.log(slicedArrayC);

// toString();
// returns an array as a string seperated by commas

let stringArray = countries.toString();
console.log('Result from toString method:');
console.log(stringArray);

// concat();
// combines two arrays and returns the combined result

let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale css', 'breathe sass'];
let taskArrayC = ['get git ', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from conCat method:');
console.log(tasks);

// combining multiple arrays
console.log('Result from conCat method:');

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// combining arrays with elements
let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log('Result from conCat method:');
console.log(combinedTasks);

// join ()
/*
returns array as a string seperated by specified separator string

*/

let users = ['John', 'Jane', 'Doe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration methods
/*
	-are loops designed to perform repetitve tasks on array
	-loops over all items in an array
	-useful for manipulating array data resulting in complex tasks
	-array iteration methods normally wrks with a function supplies as an argument
	-how these function works is by performing tasks that are pre defined within an arrays method
*/

// forEach()
// similar to a for loop that iterates one ach array element
// for each item in the array, the anonymous function passed in the forEach() methpd will be run
// the anonymous function is able to receive the current item beog iterated or loop over by assigning a parameter
// its commom practice to use the singular form of the array content for parameter name used ina rray loops


allTasks.forEach(function(task){
	console.log(task);
})

// using forEach with conditional statement

let filteredTasks = [];

allTasks.forEach(function(task){
	if (task.length > 10){
		filteredTasks.push(task);
	}
})
console.log("Result of filtered tasks: ");
console.log(filteredTasks);

// map()
/*
	-iterates on each element and returns new array with different values depending on the result of the functions operations
	-this is useful for performing taks where mutating/changing the elements are required
	-unlike the forEach method, map method requires the use of return statement in order to create another array with the performd operaiton
*/

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(numbers){
	return numbers * numbers;
})

console.log("Original array: ");
console.log(numbers);
console.log("Result of map method: ");
console.log(numberMap);

// every()
/*
	-checks if all elements in an array meet the given condition
	-this is usefule for validating data stored in arrays expecially when dealing with large amounts of data
	-returns a true value if all elements meet the condition and false if otherwise
*/

let allValid = numbers.every(function(numbers){
	return (numbers < 3);
});
console.log("Result of every method: ");
console.log(allValid);

// some()
/*
	-checks if at least one elements in an array meet the given condition
	
	-returns a true value if at least one elements meet the condition and false if otherwise

*/

let someValid = numbers.some(function(numbers){
	return (numbers < 2);
});
console.log("Result of some method: ");
console.log(someValid);

// filter()
/*
-RETURNS NEW ARRAY TAHT CONTAINS ELEMENTS WHICH MEETS THE GIVEN CONDITION
-REturns an empty array if no elements were found
*/

let filterValid = numbers.filter(function(numbers){
	return (numbers < 3 );
});
console.log("Result of filter method: ");
console.log(filterValid);

// no elements found

let nothingFound = numbers.filter(function(numbers){
	return (numbers = 0 );
});
console.log("Result of filter method: ");
console.log(nothingFound);

// filtering using forEach()
let filteredNumbers = [];

numbers.forEach(function(numbers){
	if (numbers < 3){
		filteredNumbers.push(numbers)
	}
})

console.log("Result of filter method: ");
console.log(filteredNumbers);

// includes()
/*
	-includes() method checks if the argument passed can be found in the array
	-returns a boolean which can be saved in a variable
	-returns true if arguement is found in the array otherwise false
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let productFound1 = products.includes("Mouse");
console.log(productFound1)

let productFound2 = products.includes("Headset");
console.log(productFound2)

/*
	-methods can be chained using them one after another
	-the result of the first method is used on the second method until all chained methods have been resolved
*/

let filterProducts = products.filter(function(products){
	return products.toLowerCase().includes('a');
})
console.log(filterProducts);

// reduce()
/*
	-evaluates elements from left to right and returns/reduces the array into a single value
*/

let iteration = 0;
console.log(numbers);
let reducedArray = numbers.reduce(function(x,y){
	console.warn('Current iteration ' + ++iteration);
	console.log('accumulator: ' +x);
	console.log('currentValue: ' +y);
	return x + y;
})

console.log("Result of reduce method: " + reducedArray);

// reducing string arrays
let list = ['Hello', 'Again', 'World']

let reducedJoin = list.reduce(function(x,y){
	return x + ' ' + y;
})

console.log("Result of reduce method: " + reducedJoin);