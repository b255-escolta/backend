/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime",
    
];



/*
    
   1. Create a function called register which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, return the message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and return the message:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/


function register(userName){

    let findUsers = registeredUsers.includes(userName);

    if(findUsers == 1){
        console.log('Registration failed. Username already exists!');
    }

    else {
        registeredUsers.push(userName);
        console.log('Thank you for registering!');
    }
}
register('James Jeffries');
register('Conan O' +'` ' +'Brien');
register('Conan O' +'` ' +'Brien');

console.log(registeredUsers);



/*
    2. Create a function called addFriend which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then return the message with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, return the message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
let friendsList = [];

function addFriend(friend){

    let addNew = registeredUsers.includes(friend);

    if(addNew == 1){
        friendsList.push(friend);
        console.log('You have added ' +friend +' as a friend');
    }

    else {
        console.log('User not found!');
    }
}
addFriend('Victor Magtanggol');
// addFriend('Macie West');
// addFriend('Tristan Vasco');
addFriend('Conan O' +'` ' +'Brien');

// console.log(friendsList);

/*
    3. Create a function called displayFriends which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty return the message: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function displayFriends(){
let displayAll = friendsList.length;
if(displayAll >= 1){
        console.log(friendsList);
    }
else {
        console.log('You have 0 friends. Add one first!');
    }
}
displayFriends();

/*
    4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty return the message:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty return the message:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function displayNumberOfFriends(){
let numberOfFriends = friendsList.length;
if(numberOfFriends >= 1){
        console.log("You currently have " +numberOfFriends +" friend(s).");
    }
else {
        console.log('You have 0 friends. Add one first!');
    }
}
displayNumberOfFriends();


/*
    5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty return a message:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
// console.log(registeredUsers);

function deleteFriend(){
let delete1 = friendsList.pop();
if(delete1.length != 0){
        console.log("You deleted " +delete1 +" as your friend.");
    }
else {
        console.log('You have 0 friends. Add one first!');
    }
}
deleteFriend();


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/






//For exporting to test.js
try {
    module.exports = {
        registeredUsers,
        friendsList,
        register,
        addFriend,
        displayFriends,
        displayNumberOfFriends,
        deleteFriend
    }
}
catch(err) {

}

