//require directive to load the module 


const express = require ("express")

const app = express()

// application server, need port to listen
const port = 4000;

// setup data for server to handle data from requests
app.use(express.json());

// true, allows to receive info in other data types
app.use(express.urlencoded({extended:true}));

// [SECTION ROUTES]
app.get("/", (req, res) => {
	res.send("hello world")
});

app.get("/hello", (req, res) => {
	res.send("hello from the /hello endpoint")
});

// CREATE a POST route

app.post("/hello", (req, res) => {
	res.send(`hello There ${req.body.firstName} ${req.body.lastName}`)
});

// an array that will store user objects when the "/signup" route is accessed. serve as mock database

let users = [];

app.post("/signup", (req, res) => {
	console.log (req.body);

// if contents of request.body with property userName and password is not empty
	if (req.body.userName !== '' && req.body.password !== ''){
		users.push(req.body);

		res.send(`User ${req.body.userName} successfully registered!`);
	} else {
		res.send("Please input BOTH username and password")
	}
});

// create PUT to change password

app.put("/change-password", (req, res) => {
	let message;

	for(let i = 0; i < users.length; i++){
		
		if(req.body.userName == users[i].userName){
			users[i].password = req.body.password;

			message = `User ${req.body.userName}'s password has been updated`;

			break;
		} else {
			message = "User does not exist."
		}
	}
	res.send(message);
})




// tells server to listen to port
if (require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}

module.exports = app;