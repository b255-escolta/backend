console.log("Hello World");

// [SECTION] JSON Objects
/*
	-JSON stands for JavScript Object Notation
	-JSON is also used in other programming languages hence the name JavaScript Object Notation
	-Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and coverting strings into JavaScript objects
	-JavaScript objets are not to be confused with JSON
	-Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
	-A byte is a unit of data that is eight binary digits(1 and 0) that is used to represent a character(letters, numbers or typographic symbols)
	-Bytes are information that a computer processes to perform different tasks
*/

// JSON Objects
/*
{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}
*/

// JSON ARRAYS
/*
"cities": [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
]
*/

// [SECTION] JSON methods
// - The JSON object contains methods for parsing and converting data into stringified JSON

// [SECTION] Converting Data into stringified JSON
/*
	-Stringified JSON is a JavaScript object converted into a string to be used in other functions of a JavaScript application
	-They are commonly used in HTTP requests where information is required to be sent and received in a stringified JSON format
	-Requests are an important part of programming where an application communicates with a backend application to perform different tasks such as getting/creating data in a database
	-A fronted application is an application that is used to interat with users to perform different visual tasks and display information while backend applications are commonly used for all the business logic and data processing
	-A database normally stores information/data that can be used in most applications
	-Commonly stored data in databases are user information, transaction records and product information
	-Node/Express JS are two types of technologies that are used for creating backend applications which processes requests from frontend applications
*/

let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}];

console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data);

// [SECTION] Using stringify method with Variables
/*
	-When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
	-The "property" name and "value" would have the same name which can be confusing for beginners
	-This is done on purpose for code readability meaning when and information is stored in a variable and when the object created to be stringified is created, we supply the variable name instead of a hard coded value
	-This is commonly used when the information to be stored and sent to a backend application will come from a frontend application
*/

let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your address belong to?')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);

// [SECTION] Converting stringified JSON into JavaScript objects
/*
	-Objects are common data types used in application because of the complex data structure that can be created out of them
	-Information is commonly sent to applications in stringified JSON and then converted back into objects
	-This happens both for sending information to a backend application
*/

let batchesJSON = '[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]';

console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));





