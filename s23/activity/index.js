// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

let trainer = {
	name: 'Ash Ketchum',
	age: 1999,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May','Max'],
		kanto: ['Brock','Misty']
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}


// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log('Result of bracket notation:');
console.log(trainer['pokemon']);

console.log('Result of talk method:');
trainer.talk();



// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		let update = (target.health - this.attack);

		target.health = update;

		console.log(target.name +'`s health is reduced to ' +update);

		this.faint = function(){
		console.log(this.name + ' fainted');
		}

		if (target.health > 0){
			console.log(target);
		}

		if (target.health <= 0){
			target.faint(this.name);
			console.log(target);
		}

		}

		


		
		// if (target.health <= 0){
		// 	// console.log(target.name+ ' fainted');
		// 	// console.log(target);

		// 	this.faint = function(){
		// 	console.log(target.name + 'fainted');
		// 	}
		// } 
		


	};
	

	


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

geodude.tackle(pikachu);
mewtwo.tackle(geodude);



//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}