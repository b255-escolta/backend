
// Instruction #3
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json()).then((json) => console.log(json));


// Instruction #4
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json.map((item) => {
  return item.title
})));


// Instruction #5
fetch ('https://jsonplaceholder.typicode.com/todos?id=1')
.then((response) => response.json())
.then((json) => console.log(json));

// Instruction #6
fetch('https://jsonplaceholder.typicode.com/todos?id=1')
  .then(response => response.json())
  .then(data => {
    let posts = data.map(post => ({
      
      title: post.title,
      completed: post.completed,


    }));
    console.log(posts);
  })


  // Instruction #7
  fetch ('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',

  headers: {
    'Content-type': 'application/json',
  },

  body: JSON.stringify({
    userId: 1,
    id: 201,
    title: 'Created To Do List Item',
    completed: false
  })
})
.then((response) => response.json())
.then((json) => console.log(json));


// Instruction #8

fetch ('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',

  headers: {
    'Content-type': 'application/json',
  },

  body: JSON.stringify({
    userId: 1,
    id: 1,
    title: 'Updated To Do List Item',
    dateCompleted: 'Pending',
    status: 'Pending',
    description: 'To update the my to do list with a different data structure'
  })
})
.then((response) => response.json())
.then((json) => console.log(json));

// Instruction #9
fetch ('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  headers: {
    'Content-type': 'application/json',
  },
  body: JSON.stringify({
    
    userId: 1,
    title: 'delectus aut autem',
    dateCompleted: '07/09/21',
    status: 'Complete',
    completed: false

  })
})

.then((response) => response.json())
.then((json) => console.log(json));


// Instruction #12

fetch ('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
});