console.log("Hello World")

console.log (fetch('https://jsonplaceholder.typicode.com/posts'));

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json()).then((json) => console.log(json));

async function fetchData(){

	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}

fetchData();

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

// creating a post

fetch ('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello World',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// updating using put

fetch ('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		title: 'Updated Post',
		body: 'Hello Again',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// updating using patch

fetch ('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: 'Corrected post',

	})
})

.then((response) => response.json())
.then((json) => console.log(json));

// deleting a post

fetch ('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});

// filtering posts

fetch ('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json));

// retrieving nested/related comments to posts

fetch ('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));