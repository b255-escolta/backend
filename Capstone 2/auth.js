const jwt = require("jsonwebtoken");
// user defined string data that will be used to create our JSON web tokens
// used in algorithm for encrypting our data which makes it difficult to decode the info without the defined secret keyword
const secret = "ProductBookingAPI";

// [SECTION] JSON Web Tokens
/*
	-JSON web token or JWT is a way of securely passing info from the server to the frontend or to other parts of the server
	-info is kept secure through the use of the secret code
	-only the system knows the secret code that cande decode the encrypted info
*/

// Token creation
module.exports.createAccessToken = (user) => {
	// the data will be received from the registration form
	// when the user logs in, a token will be created with the user's info
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};
	// generate a JSON web token using the JWT's sign method
	// generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
}

// token verification
module.exports.verify = (req, res, next) => {
	// the token retrieved from the request header
	// this can be provided in postman under auth>bearer token

	let token = req.headers.authorization;

	// token recieved and is not undefined
	if (typeof token !== "undefined"){
		console.log(token);
		// the "slice" method takes only the token from the info sent via the request header
		// the token sent is a type of bearer token which when recieved contains the word "bearer" as a prefix to the string
		// this removes the "bearer" prefix and obtains only the token for verification
		token = token.slice(7, token.length);

		// validate the token using the "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (err,data) => {

			// if JWT is not valid
			if(err){
				return res.send({auth : "failed"})
			} else {
				// allows the application to proceed with the next middleware function/callback function in the route
				// verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
				next();
			}
		})
		// token does not exist
	} else {
		return res.send({auth : "failed"})
	};
};

// token decryption
module.exports.decode = (token) => {
	// token received and is not undefined
	if (typeof token !== "undefined"){
		// retrieved only the token and removes the bearer prefix
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			} else {
				// the "decode" method is used to obtain the info from the jwt
				// the "{complete:true}" option allows us to return the additional info from the jwt token
				// returns an object with access to the "payload" property which contains user info stored when the token was generated
				// the payload contains info provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				return jwt.decode(token, {complete:true}.payload)
			}
		})
		// token does not exist
	} else {
		return null;
	}
}