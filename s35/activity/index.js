const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://gerry-255:admin123@zuitt-bootcamp.oeebhge.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);


let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))


const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}
})



const Task = mongoose.model("Task", taskSchema);


app.use(express.json());
// Allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

let users = [];

app.post("/signup", (req, res) => {
	console.log (req.body);

// if contents of request.body with property userName and password is not empty
	if (req.body.userName !== '' && req.body.password !== ''){
		users.push(req.body);

		res.send(`User ${req.body.userName} successfully registered!`);
	} else {
		res.send("Please input BOTH username and password")
	}
});




app.get("/tasks", (req, res) => {

	Task.find({}).then((result, err) => {

		if (err) {
			return console.log(err)
		} else {
			return res.status(200).json({
				data:result
			})
		}
	})
})





// Listen to the port, meaning, if the port is accessed we run the server
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));

}

module.exports = app;